package com.company;

public class Main {

    public static void main(String[] args) {
        ExampleMyException exampleMyException = new ExampleMyException();
        exampleMyException.exmpIndexOutOfBoundsException();
        exampleMyException.exmpNullPointerException();
        exampleMyException.exmpNumberFormatException();
        exampleMyException.exmpClassCastException();
        exampleMyException.exmpClassNotFoundException();
        exampleMyException.exmpIllegalArgumentException(-1,10);
        exampleMyException.exmpIllegalStateException();
        exampleMyException.exmpUnsupportedOperationException();
        exampleMyException.exmplStackOverflowError(1);
                    }
}