package com.company;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ExampleMyException {
    String[] category = {"Headlights", "Tail Lights", "Signal Lights"};
    int[] idCategory = new int[3];

    public void exmpNullPointerException() {
        idCategory = null;
        try {
            int i = idCategory.length;
        } catch (NullPointerException exp1) {
            System.out.println("Invalid use of null reference. " + exp1);
        }
    }

    public void exmpIndexOutOfBoundsException() {
        try {
            category[3] = "Fog Lights";
        } catch (IndexOutOfBoundsException exp2) {
            System.out.println("The array index is out of bounds. " + exp2);
        }
    }
    public void exmpNumberFormatException() {
        try {
            int value1 = Integer.parseInt(category[0]);
            System.out.println(value1);
        } catch (NumberFormatException exp3) {
            System.out.println("Invalid conversion of a string to a number format. " + exp3);
        }
    }

    public void exmpClassCastException() {
        try {
            Object id = new Integer(0);
            System.out.println((String) id);
        } catch (ClassCastException exp4) {
            System.out.println("Invalid typecasting. " + exp4);
        }
    }

    public void exmpClassNotFoundException() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            System.out.println("Class successfully loaded.");
        } catch (ClassNotFoundException exp5) {
            System.out.println("Class not successfully loaded. " + exp5);
        }
    }

    public static int exmpIllegalArgumentException(int x, int y) {
        if (x > 0 && y > 0) {
            System.out.println("Area = " + x * y);
            return x * y;
        }
        try {
            if (x < 0 || y < 0)
                throw new IllegalArgumentException("When you call the method, an illegal argument is used.");
        } catch (IllegalArgumentException exp6) {
            System.out.println(exp6);
        }
        return 0;
    }

    public void exmpIllegalStateException() {
        List<String> arrayList = new ArrayList<String>();
        arrayList.add("Subcategory1");
        arrayList.add("Subcategory2");
        Iterator<String> iterator = arrayList.iterator();
        try {
            while (iterator.hasNext()) {
                iterator.remove();
            }
        } catch (IllegalStateException exp7) {
            System.out.println("IllegalStateException. " + exp7);
        }
    }

    public void exmpUnsupportedOperationException() {
        List categoryList = Arrays.asList(category);
        try {
            categoryList.add("Fog Lights");
        } catch (UnsupportedOperationException exp8) {
            System.out.println("Add operation is not supported. " + exp8);
        }
        try {
            categoryList.remove(0);
        } catch (UnsupportedOperationException exp8) {
            System.out.println("Remove operation is not supported. " + exp8);
        }
    }
    public void exmplStackOverflowError(int n) {
        System.out.println("Number: " + n);
        exmplStackOverflowError(++n);
    }
                         }
